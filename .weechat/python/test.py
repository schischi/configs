import weechat, string

weechat.register("test", "test42", "0.2", "GPL", "gnotify: Growl notifications for Weechat", "", "")

# script options
settings = {
    "show_hilights"             : "on",
    "show_priv_msg"             : "on",
}

# Hook privmsg/hilights
weechat.hook_signal("weechat_pv", "notify_show_priv", "")

def notify_show_priv( data, signal, message ):
    weechat.command("", "/help")
    """Sends private message to be printed on notification"""
    if weechat.config_get_plugin('show_priv_msg') == "on":
        show_notification("Weechat Private Message",  message)
    return weechat.WEECHAT_RC_OK

def show_notification(title,message):
    os.execv('/usr/bin/urxvt', [])

