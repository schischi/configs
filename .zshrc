###################
#### OH MY ZSH ####
###################
ZSH=$HOME/.oh-my-zsh
#ZSH_THEME="flazz"
#ZSH_THEME="fletcherm"
ZSH_THEME="dpoggi"
#ZSH_THEME="arrow"
DISABLE_AUTO_UPDATE="true"
export UPDATE_ZSH_DAYS=13
DISABLE_AUTO_TITLE="true"
COMPLETION_WAITING_DOTS="true"
plugins=(git history-substring-search)
setopt extendedglob
source $ZSH/oh-my-zsh.sh

#################
#### ALIASES ####
#################

source $HOME/.sh_aliases
source $HOME/.sh_var
