#!/usr/bin/env python

import i3
import sys

def move_to_border(border):
    workspace = [ws for ws in i3.get_workspaces() if ws['focused']][0]
    window = i3.filter(nodes=[], focused=True)[0]
    i3.floating('enable')
    if border in ['r', 'right']:
        x = workspace['rect']['width'] - window['rect']['width'] - window['rect']['x']
        i3.move('right', str(x), 'px')
    if border in ['l', 'left']:
        x = window['rect']['x']
        i3.move('left', str(x), 'px')
    if border in ['u', 'up']:
        y = window['rect']['y']
        i3.move('up', str(y), 'px')
    if border in ['d', 'down']:
        y = workspace['rect']['height'] - window['rect']['height'] - window['rect']['y'] + workspace['rect']['y']
        i3.move('down', str(y), 'px')
    if border in ['c', 'center']:
        i3.move('absolute', 'position', 'center')


if __name__ == '__main__':
    for arg in sys.argv:
        move_to_border(arg)
