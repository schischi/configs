#!/usr/bin/env zsh

setopt extendedglob
setopt glob_dots

update() {
    git pull
    git submodule update
    git submodule foreach git pull origin master
}

install() {
    git submodule init
    git submodule update
    cd $HOME
    ln -s $OLDPWD/*~*.git~*.gitmodules~*install.zsh~*README.mkd .
}

theme() {
    mkdir .icons
    wget http://jimmac.musichall.cz/zip/vanilla-dmz-aa-0.4.tar.bz2 -P .icons/
    tar xf .icons/vanilla-dmz-aa-0.4.tar.bz2 -C .icons
    rm .icons/vanilla-dmz-aa-0.4.tar.bz2

    mkdir .themes
    wget http://box-look.org/CONTENT/content-files/136162-Crunchy-themes.tar.gz -P .themes/
    tar xf .themes/136162-Crunchy-themes.tar.gz -C .themes
    rm .themes/136162-Crunchy-themes.tar.gz

    cd $HOME
    ln -s $OLDPWD/.themes
    ln -s $OLDPWD/.icons
}

case "$1" in
    'install')
        install
        ;;
    'update')
        update
        ;;
    'theme')
        theme
        ;;
    *)
        exit 1
        ;;
esac
