set print pretty on
set prompt \001\033[01;31m\002gdb $ \001\033[0m\002

define plistc
 set $next = $arg0.head
 while ($next != 0)
  p $next
  p *$next
  set $next = $next.next
 end
end

define plist
 set $next = $arg0.head
 while ($next != 0)
  p $next
  set $next = $next.next
 end
end
