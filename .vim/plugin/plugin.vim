" Rainbow Parentheses
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound

let g:rbpt_colorpairs = [
            \ ['darkcyan',    'RoyalBlue3'],
            \ ['darkgray',    'DarkOrchid3'],
            \ ['darkgreen',   'firebrick3'],
            \ ['white',        'RoyalBlue3'],
            \ ]

" Nerd Tree
let g:NERDTreeWinSize=24

" Syntastic
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_check_on_open=0
let g:syntastic_check_on_wq=0
let g:syntastic_loc_list_height=5
let g:syntastic_auto_loc_list=1
let g:syntastic_c_check_header=1
let g:syntastic_c_compiler = 'clang'
let g:syntastic_cpp_check_header=1
let g:syntastic_cpp_include_dirs = ['.', '..', '../include','include']
let g:syntastic_cpp_compiler='clang++'
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_python_checker_args='--ignore=E128'
let g:syntastic_mode_map = { 'mode': 'active',
            \ 'active_filetypes': ['c', 'python'],
            \ 'passive_filetypes': ['cpp'] }

" Supertab
let g:SuperTabDefaultCompletionType = "context"
let g:SuperTabContextDefaultCompletionType = "<c-n>"
let g:SuperTabDefaultCompletionType="<c-n>"

