" Vim syntax file
" Language: Algo EPITA
" Maintainer: Adrien Schildknecht
" Latest Revision: 21/01/2013

if exists("b:current_syntax")
  finish
endif

" Keywords
syn keyword epiStatement retourne continue sortir
syn keyword epiKeywords si sinon fin faire debut alors algorithme pour
syn match epiKeywords "jusqu'a"
syn match epiKeywords "tant que"

" Label
syn match epiLabel "parametres [A-Za-z]*"
syn keyword epiLabel variables constantes

" Constant
syn keyword epiConstant nul nil vrai faux

" Operator
syn match epiOperator "<>"
syn match epiOperator "="
syn match epiOperator "/"
syn match epiOperator "*"
syn match epiOperator "-"
syn match epiOperator "+"
syn match epiOperator "%"
syn match epiOperator ">"
syn match epiOperator "<"
syn keyword epiOperator et ou non

" Type
syn match epiTypeDef "^\ *t_[A-Za-z0-9_]*"
syn keyword epiType entier graphe

" Function
syntax keyword epiFunction afficher

" Comment
syn match epiComment "//.*$" contains=epiTodo
syn match epiComment "/\*.*\*/" contains=epiTodo
syn keyword epiTodo contained TODO FIXME NOTE

" String
syn region epiString start='"' end='"'

" Integer
syn match epiNumber '\d\+'
syn match epiNumber '[-+]\d\+'

hi def link epiStatement Conditional
hi def link epiKeywords Conditional
hi def link epiType Type
hi def link epiTypeDef Structure
"hi def link epiLabel Label
hi def link epiLabel PreProc
hi def link epiComment     Comment
hi def link epiTodo        Todo
hi def link epiNumber      Number
hi def link epiOperator    Operator
hi def link epiFunction    Function
hi def link epiString      String
hi def link epiConstant    Constant
