call pathogen#infect()
call pathogen#helptags()

augroup filetypedetect
    " Mail
    autocmd BufRead,BufNewFile *mutt-*              setfiletype mail
augroup END
